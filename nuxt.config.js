const pkg = require('./package')

export default () => {
  return {
    mode: 'universal',

    /*
     ** Headers of the page
     */
    head: {
      title: 'Beauty Survey',
      meta: [{
          charset: 'utf-8',
        },
        {
          name: 'viewport',
          content: 'width=device-width, initial-scale=1',
        },
        {
          hid: 'description',
          name: 'description',
          content: pkg.description,
        },
      ],
      link: [{
        rel: 'icon',
        type: 'image/x-icon',
        href: '/favicon.ico',
      }, ],
    },

    /*
     ** Customize the progress-bar color
     */
    loading: {
      color: '#fff',
    },

    /*
     ** Global CSS
     */
    css: [],

    /*
     ** Plugins to load before mounting the App
     */
    plugins: [],

    /*
     ** Nuxt.js modules
     */
    modules: ['@nuxtjs/firebase', 'bootstrap-vue/nuxt'],

    // buildModules: ['@nuxt/typescript-build'],

    /*
     ** Nuxt.js Middleware
     */
    router: {
      // middleware: ['testMiddleware'],
    },

    //Nuxt-Fire Module Options
    firebase: {
      config: {
        apiKey: 'AIzaSyCwer1ufJgh-Y2wn7Y7oCEgFyUEEEJSVJ8',
        authDomain: 'aslpolls.firebaseapp.com',
        databaseURL: 'https://aslpolls.firebaseio.com',
        projectId: 'aslpolls',
        storageBucket: 'aslpolls.appspot.com',
        messagingSenderId: '330597398358',
        appId: '1:330597398358:web:f6bca5e1aa749e1faf88f7',
        measurementId: 'G-4J1B2PMG0B',
      },
      onFirebaseHosting: false,
      services: {
        // auth: {
        //   initialize: {
        //     onAuthStateChangedAction: 'onAuthStateChanged'
        //   },
        //   ssr: true
        // },
        firestore: {
          memoryOnly: false,
          static: false,
        },
        functions: {
          // emulatorPort: 12345
        },
        storage: true,
        realtimeDb: true,
        performance: true,
        analytics: true,
        // remoteConfig: {
        //   settings: {
        //     fetchTimeoutMillis: 60000,
        //     minimumFetchIntervalMillis: 43200000
        //   },
        //   defaultConfig: {
        //     welcome_message: 'Welcome'
        //   }
        // },
        // messaging: {
        //   createServiceWorker: true,
        //   actions: [{
        //     action: 'goToUrl',
        //     url: "https://github.com/lupas"
        //   }]
        // }
      },
    },

    // Fixes issues with settinmg firestore.settings()
    // render: {
    //   bundleRenderer: {
    //     runInNewContext: false
    //   }
    // },

    /*
     ** Build configuration
     */
    build: {
      /*
       ** You can extend webpack config here
       */
      extend(config, ctx) {},
    },
  }
}